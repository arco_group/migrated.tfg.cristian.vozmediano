'use strict';

let five = require("johnny-five");

module.exports = class Sensor extends five.Led {
	constructor(pin, place, defaultValue) {
		super({
			pin,
			type: 'digital'
		});

		this.state = "off";
        this._toggle = function(){
            if(this.state === "off"){
                this.state = "on";
            }else{
                this.state = "off";
            }
            this.toggle();
        };

        this._on = function(){
            this.state = "on";
            this.on();
        };

        this._off = function(){
            this.state = "off";
            this.off();
        }
	}
};
