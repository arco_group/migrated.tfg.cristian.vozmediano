'use strict';

let five = require("johnny-five");

module.exports = class Sensor extends five.Sensor {
	constructor(pin, threshold, place) {
		super({
			pin,
			threshold: 50
		});

		this.place = place || 'upfloor/#';
		
		this.on('change', function(value) {
			let current = this.scaleTo(0, 10) > threshold;
			console.log("Light sensor value --> ",this.scaleTo(0, 10));
			if (current !== this.state) {
				this.state = current;
				this.emit('toggle', current);
			}
		});
	}
};
