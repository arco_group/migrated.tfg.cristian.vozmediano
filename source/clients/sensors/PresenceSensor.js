'use strict';

let five = require("johnny-five");

module.exports = class Sensor extends five.Sensor {
	constructor(pin, place, defaultValue) {
		super({
			pin,
			type: 'digital',
			invert:true
		});

		this.state = defaultValue || 0;
		this.place = place;
		this.operative = true;
		this.on('data', function(value) {
			if(value !== this.state){
				this.state = value;
				this.emit('toggle', this.state);
			}
		});
	}
};
