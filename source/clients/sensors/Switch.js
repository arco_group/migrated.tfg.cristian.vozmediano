'use strict';

let five = require("johnny-five");

module.exports = class Sensor extends five.Button {
	constructor(pin, place, defaultValue) {
		super({
			pin,
			type: 'digital'
		});

		this.state = defaultValue || false;
		this.place = place;
		
		this.on('press', function(value) {
			this.state = !this.state;
			this.emit('toggle', this.state);
		});
	}
};
