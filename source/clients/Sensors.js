/**
 * Requires
 */
var five = require("johnny-five"),
    Switch = require('./sensors/Switch'),
    PresenceSensor = require('./sensors/PresenceSensor'),
    LightSensor = require('./sensors/LightSensor'),
    mqtt = require('mqtt'),
    config = require('../config/client.config.json'),
    cvgMqttProtocol = require('../libs/cvgMqttProtocol'),
    client,
    switchOpen = false;


/**
 * Initialization
 */
board = new five.Board({
    port:config.sensorsClient["port_" + config.sensorsClient.portSelected]
});


/**
 * Implementation
 */
board.on("ready", function () {
    var message, that = this,
        lightSensorFunction,
        presenceFunction,
        switchFunction;

    console.log("Sensors Board - Status READY");
    client = mqtt.connect(config.sensorsClient.urlBroker,{
        clientId:config.sensorsClient.clientId
    });

    this.lightSensor = new LightSensor("A0", 5);
    this.presenceSensorHall = new PresenceSensor(10, "downfloor/hall");
    this.presenceSensorBathRoom = new PresenceSensor(11, "upfloor/bathroom");
    this.switchLivingRoom = new Switch(2, "downfloor/livingroom");
    this.switchKitchen = new Switch(4, "downfloor/kitchen");
    this.switchBedRoom = new Switch(7, "upfloor/bedroom");
    this.switchStudy = new Switch(8, "upfloor/study");

    this.lightSensor.on("toggle", lightSensorFunction);
    this.presenceSensorHall.on("toggle", presenceFunction);
    this.presenceSensorBathRoom.on("toggle", presenceFunction);
    this.switchLivingRoom.on("toggle", switchFunction);
    this.switchKitchen.on("toggle", switchFunction);
    this.switchBedRoom.on("toggle", switchFunction);
    this.switchStudy.on("toggle", switchFunction);

    client.on('connect', function () {
        client.subscribe('cvgmqttprotocol/system',{qos:0});
        that.operative = true;
    });

    client.on('message', function (topic, message) {
        var message = message.toString().toUpperCase();

        switch(topic){
            case "cvgmqttprotocol/system":
                if(message === "ON" || message === "AUTOMATIC"){
                    console.log("System -> OPERATIVE");
                    that.operative = true;
                }else if(message === "OFF" || message === "MANUAL"){
                    console.log("System -> NO OPERATIVE");
                    that.operative = false;
                }else if(message === "SENSORLIGHT_DOWNFLOOR"){
                    console.log("SensorLight -> DOWNFLOOR");
                    that.lightSensor.place = 'downfloor/#';
                }else if(message === "SENSORLIGHT_UPFLOOR"){
                    console.log("SensorLight -> UPFLOOR");
                    that.lightSensor.place = 'upfloor/#';
                }else if(message === "SENSORLIGHT_ALL"){
                    console.log("SensorLight -> ALL");
                    that.lightSensor.place = '#'
                }else if(message === "SENSORLIGHT_OFF"){
                    console.log("SensorLight -> OFF");
                    that.lightSensor.place = null;
                }else if(message === "SENSORPRESENCE_DOWNFLOOR/HALL_ON"){
                    console.log("SensorPresence Hall -> ON");
                    that.presenceSensorHall.operative = true;
                }else if(message === "SENSORPRESENCE_DOWNFLOOR/HALL_OFF"){
                    console.log("SensorPresence Hall -> OFF");
                    that.presenceSensorHall.operative = false;
                }else if(message === "SENSORPRESENCE_UPFLOOR/BATHROOM_ON"){
                    console.log("SensorPresence Bathroom -> ON");
                    that.presenceSensorBathRoom.operative = true;
                }else if(message === "SENSORPRESENCE_UPFLOOR/BATHROOM_OFF"){
                    console.log("SensorPresence Bathroom -> OFF");
                    that.presenceSensorBathRoom.operative = false;
                }
            break;
        }
    });

    function lightSensorFunction(value) {
        console.log("Sensor light value -> ",value);
        var message;

        if(that.operative){
            if(value){
                if(this.place) message = cvgMqttProtocol.createMessage('cvgmqttprotocol/sensorlight/house1/' + this.place, 'HIGH');
                if(message) client.publish(message.topic, message.payload, message.options);
            }else{
                if(this.place) message = cvgMqttProtocol.createMessage('cvgmqttprotocol/sensorlight/house1/' + this.place, 'LOW');
                if(message) client.publish(message.topic, message.payload, message.options);
            }
        }
    };

    function presenceFunction(value) {
        var message;
        console.log("Presence value --> ", value);
        if(that.operative && this.operative){
            if(value === 0){
                message = cvgMqttProtocol.createMessage('cvgmqttprotocol/sensorpresence/house1/' + this.place, 'TRUE');
                if(message) client.publish(message.topic, message.payload, message.options);
            }else{
                message = cvgMqttProtocol.createMessage('cvgmqttprotocol/sensorpresence/house1/' + this.place, 'FALSE');
                if(message) client.publish(message.topic, message.payload, message.options);
            }
        }
    };

    function switchFunction(switchOpen){
        var message;
        console.log("Switch button change --> ");
        if(that.operative){
            message = cvgMqttProtocol.createMessage('cvgmqttprotocol/switch/house1/' + this.place, 'CHANGE');
            if(message) client.publish(message.topic, message.payload, message.options);
        }
    }

});
