/**
 * Requires
 */
var five = require("johnny-five"),
    mqtt = require('mqtt'),
    _ = require('underscore'),
    Led = require('./actuators/Led'),
    cvgMqttProtocol = require('../libs/cvgMqttProtocol'),
    config = require('../config/client.config.json'),
    client,
    board;

/**
 * Initialization
 */
board = new five.Board({
    port:config.actuatorsClient["port_" + config.actuatorsClient.portSelected]
});

/**
 * Implementation
 */
board.on("ready", function () {
    var that = this;

    console.log("Sensors Board - Status READY");

    client = mqtt.connect(config.actuatorsClient.urlBroker,{
        clientId:config.actuatorsClient.clientId
    });

    this.lights = {
        "bathroom": new Led(11),
        "study": new Led(10),
        "bedroom": new Led(9),
        "hall": new Led(6),
        "kitchen": new Led(5),
        "livingroom": new Led(3),
    }


    client.on('connect', function () {
        client.subscribe('cvgmqttprotocol/sensorlight/house1/downfloor/hall',{qos:0});
        client.subscribe('cvgmqttprotocol/sensorlight/house1/downfloor/livingroom',{qos:0});
        client.subscribe('cvgmqttprotocol/sensorlight/house1/downfloor/kitchen',{qos:0});
        client.subscribe('cvgmqttprotocol/sensorlight/house1/upfloor/bathroom',{qos:0});
        client.subscribe('cvgmqttprotocol/sensorlight/house1/upfloor/study',{qos:0});
        client.subscribe('cvgmqttprotocol/sensorlight/house1/upfloor/bedroom',{qos:0});
        client.subscribe('cvgmqttprotocol/sensorpresence/#',{qos:0});
        client.subscribe('cvgmqttprotocol/switch/#',{qos:0});
        client.subscribe('cvgmqttprotocol/light/#',{qos:0});
        client.subscribe('cvgmqttprotocol/system',{qos:0});
        that.operative = true;
    });

    client.on('message', function (topic, message) {
        var message = message.toString(), changeMessage;

        console.log(topic + ' ' +message.toString());
        switch(topic){
            case 'cvgmqttprotocol/system':
                if(message === 'ON'){
                    that.operative = true;
                }else if(message === 'OFF'){
                    that.operative = false;
                }
            break;
            case 'cvgmqttprotocol/sensorlight/house1/downfloor/hall':
            case 'cvgmqttprotocol/sensorpresence/house1/downfloor/hall':
            case 'cvgmqttprotocol/light/house1/downfloor/hall':
                lightChange.call(that, 'hall', message);
                changeMessage = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'LIGHTCHANGE_DOWNFLOOR/HALL_' + that.lights['hall'].state.toUpperCase());
                if(changeMessage) client.publish(changeMessage.topic, changeMessage.payload, changeMessage.options);
            break;
            case 'cvgmqttprotocol/sensorlight/house1/downfloor/livingroom':
            case 'cvgmqttprotocol/switch/house1/downfloor/livingroom':
            case 'cvgmqttprotocol/light/house1/downfloor/livingroom':
                lightChange.call(that, 'livingroom', message);
                changeMessage = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'LIGHTCHANGE_DOWNFLOOR/LIVINGROOM_' + that.lights['livingroom'].state.toUpperCase());
                if(changeMessage) client.publish(changeMessage.topic, changeMessage.payload, changeMessage.options);
            break;
            case 'cvgmqttprotocol/sensorlight/house1/downfloor/kitchen':
            case 'cvgmqttprotocol/switch/house1/downfloor/kitchen':
            case 'cvgmqttprotocol/light/house1/downfloor/kitchen':
                lightChange.call(that, 'kitchen', message);
                changeMessage = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'LIGHTCHANGE_DOWNFLOOR/KITCHEN_' + that.lights['kitchen'].state.toUpperCase());
                if(changeMessage) client.publish(changeMessage.topic, changeMessage.payload, changeMessage.options);
            break;
            case 'cvgmqttprotocol/sensorlight/house1/upfloor/bathroom':
            case 'cvgmqttprotocol/sensorpresence/house1/upfloor/bathroom':
            case 'cvgmqttprotocol/light/house1/upfloor/bathroom':
                lightChange.call(that, 'bathroom', message);
                changeMessage = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'LIGHTCHANGE_UPFLOOR/BATHROOM_' + that.lights['bathroom'].state.toUpperCase());
                if(changeMessage) client.publish(changeMessage.topic, changeMessage.payload, changeMessage.options);
            break;
            case 'cvgmqttprotocol/sensorlight/house1/upfloor/study':
            case 'cvgmqttprotocol/switch/house1/upfloor/study':
            case 'cvgmqttprotocol/light/house1/upfloor/study':
                lightChange.call(that, 'study', message);
                changeMessage = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'LIGHTCHANGE_UPFLOOR/STUDY_' + that.lights['study'].state.toUpperCase());
                if(changeMessage) client.publish(changeMessage.topic, changeMessage.payload, changeMessage.options);
            break;
            case 'cvgmqttprotocol/sensorlight/house1/upfloor/bedroom':
            case 'cvgmqttprotocol/switch/house1/upfloor/bedroom':
            case 'cvgmqttprotocol/light/house1/upfloor/bedroom':
                lightChange.call(that, 'bedroom', message);
                changeMessage = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'LIGHTCHANGE_UPFLOOR/BEDROOM_' + that.lights['bedroom'].state.toUpperCase());
                if(changeMessage) client.publish(changeMessage.topic, changeMessage.payload, changeMessage.options);
            break;
        }
    });

});


function lightChange(place, typeChange){
    if(this.operative === true){
        switch (typeChange) {
            case "HIGH":
            case "TRUE":
            case "ON":
                    this.lights[place]._on();
                break;
            case "LOW":
            case "FALSE":
            case "OFF":
                this.lights[place]._off();
                break;
            case "CHANGE":
                this.lights[place]._toggle();
                break;
        }
    }
}
