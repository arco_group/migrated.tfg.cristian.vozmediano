/**
 * Requires
 */

'use strict';

var mongodb = require('mongodb'),
	moment = require('moment'),
	hmm = require('nodehmm'),
	_ = require('underscore'),
    config = require('../config/broker.config.json'),
	iaData = require('../libs/IAData.js');

mongodb.MongoClient.connect(config.eventRegistry.iaRegistryDbUrl)
.then(function(db) {
    var data = db.collection("eventsCollection").find().toArray(function(err, data){
        var signals,observedData, nextData;

		signals = transformSignals(data);

		if(signals.length > 1){
			observedData = transformSignalsToStates(signals);
			nextData = searchNextStates(observedData.observations);
			console.log("Observations (Place where user stay) ----> ", observedData.states);

			if(nextData.nextPlace !== undefined && nextData.nextState !== undefined){
				console.log("Observations (Place where user will stay) ----> ",nextData.nextPlace);
				console.log("The next most likely state is --> ", nextData.nextState);
			}else{
				console.log("Sorry.. unable to perform prediction, error in the data because the sequence of observations is invalid ");
			}

		}else{
			console.log("Sorry.. unable to perform prediction, insufficient data");
		}

    });

});

function transformSignals(data){
	var event, dateEvent, lastRegistry,
    events = [{
        TIME: moment().startOf('day').format("DD/MM/YYYY HH:mm:ss"),
        HALL: false,
        BEDROOM: false,
        LIVING_ROOM: false,
        KITCHEN: false,
		BATHROOM: false,
		STUDY: false
    }];

	function _addEvent(){
		events.push(Object.assign({},events[events.length-1]));
		lastRegistry = events[events.length-1];
		lastRegistry.TIME = event.time;
	};

    for(var i = 0; i < data.length; i++){


		event = data[i];
		dateEvent = moment(event.time, "DD/MM/YYYY HH:mm:ss");

		if(moment(dateEvent).isSame(moment(), 'day')){


			switch(event.topic){
				case 'cvgmqttprotocol/sensorpresence/house1/downfloor/hall':
					_addEvent();
					lastRegistry.HALL = !lastRegistry.HALL;
					_updateRegistry(lastRegistry, "HALL");
				break;
				case 'cvgmqttprotocol/sensorpresence/house1/upfloor/bathroom':
					_addEvent();
					lastRegistry.BATHROOM = !lastRegistry.BATHROOM;
					_updateRegistry(lastRegistry, "BATHROOM");
				break;
				case 'cvgmqttprotocol/switch/house1/downfloor/livingroom':
					_addEvent();
					lastRegistry.LIVING_ROOM = !lastRegistry.LIVING_ROOM;
					_updateRegistry(lastRegistry, "LIVING_ROOM");
				break;
				case 'cvgmqttprotocol/switch/house1/downfloor/kitchen':
					_addEvent();
					lastRegistry.KITCHEN = !lastRegistry.KITCHEN;
					_updateRegistry(lastRegistry, "KITCHEN");
				break;
				case 'cvgmqttprotocol/switch/house1/upfloor/bedroom':
					_addEvent();
					lastRegistry.BEDROOM = !lastRegistry.BEDROOM;
					_updateRegistry(lastRegistry, "BEDROOM");
				break;
				case 'cvgmqttprotocol/switch/house1/upfloor/study':
					_addEvent();
					lastRegistry.STUDY = !lastRegistry.STUDY;
					_updateRegistry(lastRegistry, "STUDY");
				break;
			}
		}
    }

    return events;
}

//Transforma cada uno de los estados del circuito al lugar donde esta la persona (observacion)
function transformSignalsToStates(data){
	var states, observations;

	states = _.map(data, function(item){
		for(var key in item){
			if(item[key] === true){
				return key;
			}
		}
	});

	observations = _.map(states, function(item){
		return iaData.observations[item];
	})


	states = _.without(states, undefined);
	observations = _.without(observations, undefined);

	if(states.length > 3 && observations.length > 3){
		states = states.slice(states.length - 3,states.length);
		observations = observations.slice(observations.length - 3,observations.length);
	}

	return {
		states,
		observations
	};
}

function searchNextStates(observations){
	var model = new hmm.Model(),
		states = Object.keys(iaData.states),
		probability = null,
		bestObservation,
		nextState;

	model.setStatesSize(states.length);
	model.setStartProbability(iaData.statesStartProbability);
	model.setTransitionProbability(iaData.transitionProbabilityMatrix);
	model.setEmissionProbability(iaData.emmisionProbabilityMatrix);

	for(var key in iaData.observations){
		var _observations, result, alpha = [];

		_observations = _.clone(observations)
		_observations.push(iaData.observations[key]);

		for (var i = 0; i < states.length; i++) {
		    alpha[i] = [];
		}
		result = hmm.forward(model, _observations, alpha);

		if(result !== -Infinity && result !== Infinity ){
			if(probability === null || result < probability){
				probability = result;
				bestObservation = key;
			}
		}
	}

	observations.push(iaData.observations[bestObservation]);
	nextState = hmm.viterbi(model, observations);
	nextState = nextState.map(function(r){return states[r]});

	return {
		nextState: nextState[nextState.length - 1],
		nextPlace: bestObservation
	};
}

function _updateRegistry(registry, observation){
	for(var key in registry){
		if(key !== observation){
			registry[key] = false;
		}
	}
}
