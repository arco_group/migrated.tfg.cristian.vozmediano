/*
 * Requires
 */
var express = require('express'),
    bodyParser = require('body-parser'),
    brokerMqtt = require('../broker/Broker'),
    app = express();


/*
 * Initialization
 */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


/*
 * Routing
 */
app.get('/', function (req, res) {
    res.send('Web Server -> Plataforma de bajo coste para la monitorización y control de entornos inteligentes');
});

app.get('/tfg-api/system', function (req, res) {
    var data = brokerMqtt.system();
    res.status(200).send({
        devices:data.devices,
        status:data.status
    });
});

app.post('/tfg-api/system', function (req, res) {
    console.log("action", req.body);
    var status = brokerMqtt.system({
        action:req.body.action,
        place: req.body.place
    });
    res.status(200).send({
        message:'system action success',
        operative:status
    });
});

app.post('/tfg-api/light-control', function (req, res) {
    var status = brokerMqtt.lightControl({
        action:req.body.action,
        place: req.body.place
    });
    res.status(200).send({
        message:'light control success',
        operative:status
    });
});

app.post('/tfg-api/sensorlight-control', function (req, res) {
    var status = brokerMqtt.sensorlightControl({
        action:req.body.action,
        place: req.body.place
    });
    res.status(200).send({
        message:'light control success',
        operative:status
    });
});

app.post('/tfg-api/presence-control', function (req, res) {
    var status = brokerMqtt.presenceControl({
        action:req.body.action,
        place: req.body.place
    });
    res.status(200).send({
        message:'system action success',
        operative:status
    });
});

app.listen(3000, function () {
    console.log('TFG HTTP Web Server listening on port 3000');
    brokerMqtt.run();
});
