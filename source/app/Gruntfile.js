"use strict";

module.exports = function (grunt) {
    
    grunt.initConfig({
        
        pkg: grunt.file.readJSON("package.json"),

        bower: {
            target: {
                rjsConfig: "src/js/config.js",
                options: {
                    baseUrl: "src/js",
                    exclude: ["almond", "requirejs"]
                }
            }
        },

        requirejs: {
            compile: {
                options: {
                    name: "init",
                    mainConfigFile: "src/js/config.js",
                    insertRequire: ["init"],
                    out: "target/js/init.js",
                    optimize: "none"
                }
            }
        },

        handlebars: {
            compile: {
                options: {
                    namespace: "JST",
                    amd: true,
                    processName: function (filePath) {
                        return filePath.split("/templates/")[1].replace(".hbc", "");
                    }
                },
                files: {
                    "src/js/templates.js": "src/js/templates/**/*.hbc"
                }
            }
        },

        concat: {
            options: {
                separator: ";",
            },
            plain: {
                src: ["bower_components/almond/almond.js", "target/js/init.js"],
                dest: "target/js/build.js"
            }
        },
        uglify: {
            all: {
                files: {
                    "target/js/build.min.js": ["target/js/build.js"]
                }
            }
        },
        clean: {
            build: {
                src: ['target/**/*']
            }
        },
        copy: {
            assets: {
                expand: true,
                cwd: "src",
                src: "assets/**",
                dest: "target/"
            },
            indexWeb:{
                expand: true,
                cwd: "src/",
                src: ["index.html"],
                dest: "target/",
                rename: function(destBase, destPath) {
                    return destBase+destPath.replace('index', 'index');
                }
            },
            indexMobile:{
                expand: true,
                cwd: "src/",
                src: ["index-mobile.html"],
                dest: "target/",
                rename: function(destBase, destPath) {
                    return destBase+destPath.replace('index-mobile', 'index');
                }
            }
        },
        express: {
            default_option: {}
        }
    });

    grunt.loadNpmTasks("grunt-bower-requirejs");
    grunt.loadNpmTasks("grunt-contrib-requirejs");
    grunt.loadNpmTasks("grunt-contrib-handlebars");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");

    grunt.registerTask("build:web", ["clean:build", "bower", "handlebars", "copy:indexWeb", "requirejs", "concat:plain", "copy:assets"]);
    grunt.registerTask("build:mobile", ["clean:build", "bower", "handlebars", "copy:indexMobile", "requirejs", "concat:plain", "copy:assets"]);

    grunt.registerTask('server', ['express','watch']);
};
