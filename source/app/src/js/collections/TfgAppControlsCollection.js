define([
    "app",
    "backbone.marionette",
    "models/TfgAppControlModel",
    "underscore"
], function(TfgApp,
		Marionette,
		TfgAppControlModel,
		_){
    "use strict";

    var Collection = Backbone.Collection.extend({

        model:TfgAppControlModel

    });

    return Collection;
});
