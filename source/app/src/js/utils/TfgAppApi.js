define([
    "app"
], function(TfgApp){
    "use strict";

    var TfgAppApi = {
        get: function(params){
            var self = this;

            console.log("api get", params);

            $.ajax({
                async:          false,
                cache:          false,
                crossDomain:    true,
                type:           "GET",
                timeout:        5000,
                dataType:       "json",
                url:            TfgApp.model.get("serverUrl") + "/tfg-api/" + params.partialUrl,
                success: function(data){
                    params.onSuccess(data);
                },
                error: function(xhr, ajaxOptions, thrownError){
                    params.onError();
                }
            });
        },

        post: function(params){
            var self = this;

            if (typeof params.onSuccess !== "undefined")  { this.onSuccess = params.onSuccess;}
            if (typeof params.onError !== "undefined")  { this.onError = params.onError;}

            $.ajax({
                async:          false,
                cache:          false,
                crossDomain:    true,
                type:           "POST",
                timeout:        5000,
                dataType:       "json",
                data:           params.data,
                url:            TfgApp.model.get("serverUrl") + "/tfg-api/" + params.partialUrl,
                success: function(data){
                    params.onSuccess();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    self.onError();
                }
            });
        }
    };

    return TfgAppApi;
});
