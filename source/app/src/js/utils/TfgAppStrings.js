define(["app"], function(TfgApp){

	var strings = {
		es:{
			"system": "Sistema",
			"controls": "Controles",
			"configuration": "Configuración",
			"on": "Encender",
			"off": "Apagar",
			"automatic": "Automático",
			"manual": "Manual",
			"change": "Cambiar",
			"sensorlight-control": "Control de Luminosidad:",
			"sensorlight-control-off": "Control de Luminosidad desactivado",
			"presence-control-hall": "Control de Presencia Hall",
			"presence-control-bathroom": "Control de Presencia Baño",
			"update": "Actualizar",
			"downfloor": "Planta baja",
			"upfloor": "Planta superior",
			"all": "Toda la casa",
			"light-control": "Luces",
			"light-control-hall":"Luz Entrada",
			"light-control-livingroom":"Luz Salón-Comedor",
			"light-control-kitchen":"Luz Cocina",
			"light-control-bathroom":"Luz Baño",
			"light-control-study":"Luz Estudio",
			"light-control-bedroom":"Luz Dormitorio",
			"urlServerText": "Url Servidor"
		}

	};

	return {
		translate:function(key){
			var language = TfgApp.model.get('language');
			return strings[language][key];
		}
	};

})
