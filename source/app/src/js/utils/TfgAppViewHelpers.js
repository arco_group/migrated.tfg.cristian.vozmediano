define(["enums/TfgAppEnums"], function(TfgAppEnums){

	return {
		isOn: function(status){
			return status === TfgAppEnums.status.ON;
		},

		isOff: function(status){
			return status === TfgAppEnums.status.OFF;
		},
	}

})
