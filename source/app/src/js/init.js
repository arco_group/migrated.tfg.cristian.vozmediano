define(["app",
        "backbone.marionette",
        "models/TfgAppModel",
        "views/TfgAppLayoutView",
        "views/TfgAppSystemView",
        "controllers/TfgAppController"
   ],

    function(
        App,
        Marionette,
        TfgAppModel,
        TfgAppLayoutView,
        TfgAppSystemView,
        TfgAppController){

        "use strict";

        var TfgApp = App;

        TfgApp.addRegions({
            content: "#app-container"
        });

        function onDeviceReady(){
            TfgApp.nameApp = "com.tfg.cvg";
            TfgApp.content.show(new TfgAppLayoutView());
            TfgApp.trigger("change:content", {
                view:new TfgAppSystemView()
            });
        };

        TfgApp.addInitializer(function() {
            this.model = new TfgAppModel();
            this.controller = new TfgAppController();
            this.controller.initializeData();
            onDeviceReady();
        });
        
        TfgApp.start();

        return TfgApp;

    }
);
