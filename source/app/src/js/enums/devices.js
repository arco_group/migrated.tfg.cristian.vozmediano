define([], function(){

	return {
        "collection": [
            {
                controlType:"light-control",
                label: "light-control-hall",
                place:"downfloor/hall",
                status:"off"
            },
            {
                controlType:"light-control",
                label: "light-control-livingroom",
                place:"downfloor/livingroom",
                status:"off"
            },
            {
                controlType:"light-control",
                label: "light-control-kitchen",
                place:"downfloor/kitchen",
                status:"off"
            },
            {
                controlType:"light-control",
                label: "light-control-bathroom",
                place:"upfloor/bathroom",
                status:"off"
            },
            {
                controlType:"light-control",
                label: "light-control-study",
                place:"upfloor/study",
                status:"off"
            },
            {
                controlType:"light-control",
                label: "light-control-bedroom",
                place:"upfloor/bedroom",
                status:"off"
            },
            {
                controlType:"sensorlight-control",
                label: "sensorlight-control",
                place:"upfloor",
                status:"on"
            },
            {
                controlType:"presence-control",
                label: "presence-control-hall",
                place:"downfloor/hall",
                status:"on"
            },
            {
                controlType:"presence-control",
                label: "presence-control-bathroom",
                place:"upfloor/bathroom",
                status:"on"
            }
        ]
	}

});
