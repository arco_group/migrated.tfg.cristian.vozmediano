define([
    "app",
    "backbone.marionette",
    "utils/TfgAppStrings",
    "underscore",
    "templates"
], function(TfgApp,
        Marionette,
        TfgAppStrings,
        _,
        JST){
    "use strict";

    var View = Marionette.CompositeView.extend({

        template: JST["tabs"],

        events:{
            "click .tab":"onClickTab"
        },

        templateHelpers:function(){
            return {
                system: TfgAppStrings.translate("system"),
                configuration: TfgAppStrings.translate("configuration")
            };
        },

        onClickTab:function(e){
            var tab = e.currentTarget.getAttribute('data-tab');
            this.activeTab(e.currentTarget.parentNode);
            TfgApp.trigger("select:tab",{
                tab:tab
            });
        },

        activeTab:function(currentElement){
            this.$el.find('li').removeClass('active');
            $(currentElement).addClass('active');
        }
    });

    return View;
});
