define([
    "app",
    "backbone.marionette",
    "views/TfgAppControlItemView",
    "underscore",
    "templates"
], function(TfgApp,
        Marionette,
        TfgAppControlItemView,
        _,
        JST){
    "use strict";

    var View = Marionette.CompositeView.extend({

        template: JST["controls"],

        childViewContainer: "#controls-container",
        childView: TfgAppControlItemView
    });

    return View;
});
