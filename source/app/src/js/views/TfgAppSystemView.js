define([
    "app",
    "backbone.marionette",
    "views/TfgAppControlsView",
    "enums/TfgAppEnums",
    "utils/TfgAppStrings",
    "utils/TfgAppViewHelpers",
    "collections/TfgAppControlsCollection",
    "underscore",
    "templates"
], function(TfgApp,
        Marionette,
        TfgAppControlsView,
        TfgAppEnums,
        TfgAppStrings,
        TfgAppViewHelpers,
        TfgAppControlsCollection,
        _,
        JST){
    "use strict";

    var View = Marionette.LayoutView.extend({

        template: JST["system"],

        ui:{
            "buttonStatus":"#status",
            "buttonMode":"#mode"
        },

        events:{
            "click #mode": "onButtonModeClick",
            "click #status": "onButtonStatusClick",
            "click #update": "onButtonUpdateClick"
        },

        regions:{
            systemControls: '.system-controls'
        },

        initialize:function(){
            this.model = TfgApp.model.get('systemModel');
        },

        templateHelpers:function(){
            var _actionStatus, _actionMode;

            if(TfgAppViewHelpers.isOn(this.model.get('status'))){
                _actionStatus = TfgAppStrings.translate("off");
            }else{
                _actionStatus = TfgAppStrings.translate("on");
            }

            return {
                name: TfgAppStrings.translate("system"),
                statusButtonText: _actionStatus,
                updateButtonText: TfgAppStrings.translate("update")
            };
        },

        onRender:function(){
            var lightControls = TfgApp.model.get('controlsCollection').filter({controlType:"light-control"});

            this.systemControls.show(new TfgAppControlsView({
                collection:new TfgAppControlsCollection(lightControls)
            }));
        },

        onButtonStatusClick:function(){
            var _action, that = this;
            TfgAppViewHelpers.isOn(this.model.get('status')) ? _action = "off" : _action = "on";

            TfgApp.trigger("api-post",{
                data:{
                    action:_action
                },
                partialUrl:"system",
                onSuccess:function(){
                    if(TfgAppViewHelpers.isOn(that.model.get('status'))){
                        that.model.set("status", TfgAppEnums.status.OFF);
                        that.ui.buttonStatus.html(TfgAppStrings.translate("on"));
                    }else{
                        that.model.set("status", TfgAppEnums.status.ON);
                        that.ui.buttonStatus.html(TfgAppStrings.translate("off"));
                    }
                    that.render();
                },
                onError:function(){
                    console.log("Error api");
                }
            });
        },

        onButtonUpdateClick:function(){
            var that = this;
            TfgApp.trigger("api-get",{
                model:this.model,
                partialUrl:"system",
                onSuccess:function(data){
                    TfgApp.trigger("refresh", data);
                },
                onError:function(){
                    console.log("Error api");
                }
            });
        }
    });

    return View;
});
