define([
    "app",
    "backbone.marionette",
    "underscore",
    "templates",
    "utils/TfgAppStrings",
    "views/TfgAppControlsView",
    "collections/TfgAppControlsCollection"
], function(TfgApp, Marionette, _, JST, TfgAppStrings, TfgAppControlsView, TfgAppControlsCollection){
    "use strict";

    var View = Marionette.LayoutView.extend({

        template: JST["configuration"],
        events:{
            "click #update":"onUpdateUrlServer"
        },

        regions:{
            systemControls: '.system-controls'
        },

        ui:{
            input: '#inputUrl'
        },

        initialize:function(options){
            this.configurationControlsCollection = options.configurationControlsCollection;
            this.urlServer = options.urlServer;
        },

        templateHelpers:function(){
            return {
                update: TfgAppStrings.translate("update"),
                urlServerText: TfgAppStrings.translate("urlServerText")
            };
        },

        onRender:function(){
            this.systemControls.show(new TfgAppControlsView({
                collection:new TfgAppControlsCollection(this.configurationControlsCollection)
            }));

            this.ui.input.val(this.urlServer);
        },

        onUpdateUrlServer: function(){
            console.log("IP Server ---> ", this.ui.input.val());
            TfgApp.trigger("update:urlServer",{
                urlServer:this.ui.input.val()
            });
        }
    });

    return View;
});
