define([
    "app",
    "backbone.marionette",
    "enums/TfgAppEnums",
    "utils/TfgAppViewHelpers",
    "utils/TfgAppStrings",
    "underscore",
    "templates"
], function(TfgApp,
        Marionette,
        TfgAppEnums,
        TfgAppViewHelpers,
        TfgAppStrings,
        _,
        JST){
    "use strict";

    var View = Marionette.ItemView.extend({

        template: JST["control"],

        ui:{
            "button":"button"
        },

        events:{
            "click button":"onButtonClick"
        },

        templateHelpers:function(){
            var _action, _label;

            if(TfgAppViewHelpers.isOn(this.model.get('status'))){
                _action = TfgAppStrings.translate("off");
            }else{
                _action = TfgAppStrings.translate("on");
            }

            if(this.model.get("controlType") === "sensorlight-control"){
                if(!TfgAppViewHelpers.isOn(this.model.get('status'))){
                    _label = TfgAppStrings.translate("sensorlight-control-off");
                }else{
                    _label = TfgAppStrings.translate(this.model.get('label')) + " ";
                    _label += TfgAppStrings.translate(this.model.get('place'));
                }

            }else{
                _label = TfgAppStrings.translate(this.model.get('label'));
            }

            return {
                action: _action,
                label: _label,
                controlType: this.model.get("controlType"),
                isSensorLightControl:this.model.get("controlType") === "sensorlight-control",
                change: TfgAppStrings.translate('change')
            };
        },

        onRender:function(){
            if(TfgAppViewHelpers.isOff(TfgApp.model.get('systemModel').get('status')) ){
                this.ui.button.attr("disabled","disabled");
            }
        },

        onButtonClick:function(){
            var _action, _place, that = this;

            if(this.model.get("controlType") === "sensorlight-control"){
                _action = this._getSensorLightControlData()["action"];
                _place = this._getSensorLightControlData()["place"];
            }else{
                TfgAppViewHelpers.isOn(this.model.get('status')) ? _action = "off" : _action = "on";
                _place = that.model.get("place");
            }

            TfgApp.trigger("api-post",{
                data:{
                    action: _action,
                    place: _place
                },
                partialUrl:that.model.get('controlType'),
                onSuccess:function(){
                    if(that.model.get("controlType") === "sensorlight-control"){
                        that.model.set("place", _place);
                        that.model.set("status", _action);
                    }else{
                        if(TfgAppViewHelpers.isOn(that.model.get('status'))){
                            _action = "on";
                            that.model.set("status", TfgAppEnums.status.OFF);
                            that.ui.button.html(TfgAppStrings.translate("on"));
                        }else{
                            _action = "off";
                            that.model.set("status", TfgAppEnums.status.ON);
                            that.ui.button.html(TfgAppStrings.translate("off"));
                        }
                    }
                    that.render();
                },
                onError:function(){
                    console.log("Error");
                }
            });
        },

        _getSensorLightControlData:function(){
            var initPosition, nextPosition, _place, _action,
                possibleActions = ["downfloor", "upfloor", "all", "off"];

            initPosition = possibleActions.indexOf(this.model.get("place"));

            if(initPosition === (possibleActions.length - 1)){
                nextPosition = 0;
            }else{
                nextPosition = initPosition + 1;

            }

            if(possibleActions[nextPosition] === "off"){
                _place = "none";
                _action = "off";
            }else{
                _place = possibleActions[nextPosition];
                _action = "on";
            }

            return {
                action: _action,
                place: _place
            };

        }
    });

    return View;
});
