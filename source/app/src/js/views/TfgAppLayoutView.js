define([
    "app",
    "backbone.marionette",
    "views/TfgAppTabsView",
    "underscore",
    "templates"
], function(TfgApp, Marionette, TfgAppTabsView, _, JST){
    "use strict";

    var View = Marionette.LayoutView.extend({

        template: JST["layout"],

        regions:{
            tabs:'#tabs',
            content:'#content'
        },

        initialize:function(){
            this.listenTo(TfgApp, "change:content", _.bind(this.changeContent, this));
        },

        onShow:function(){
            this.tabs.show(new TfgAppTabsView());
        },

        changeContent:function(options){
            this.content.show(options.view);
        }
    });

    return View;
});
