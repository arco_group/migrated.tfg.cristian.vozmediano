define([
    "app",
    "backbone.marionette",
    "views/TfgAppSystemView",
    "views/TfgAppConfigurationView",
    "models/TfgAppSystemModel",
    "collections/TfgAppControlsCollection",
    "utils/TfgAppApi",
    "enums/devices"
], function(TfgApp,
        Marionette,
        TfgAppSystemView,
        TfgAppConfigurationView,
        TfgAppSystemModel,
        TfgAppControlsCollection,
        TfgAppApi,
        Devices){

    "use strict";

    var Controller = Marionette.Controller.extend({
        name: "tfg-app-controller",

        initialize:function(options){
            this.listenTo(TfgApp, "api-get", _.bind(TfgAppApi.get, this));
            this.listenTo(TfgApp, "api-post", _.bind(TfgAppApi.post, this));
            this.listenTo(TfgApp, "select:tab", _.bind(this.selectTab, this));
            this.listenTo(TfgApp, "update:urlServer", _.bind(this.updateUrlServer, this));
            this.listenTo(TfgApp, "refresh", _.bind(this.refresh, this));
        },

        initializeData:function(data){
            TfgApp.model.set('systemModel', new TfgAppSystemModel(
                {
                    status:"on"
                }
            ));

            if(data){
                TfgApp.model.set('controlsCollection', new TfgAppControlsCollection(data.devices));
            }else{
                TfgApp.model.set('controlsCollection', new TfgAppControlsCollection(Devices.collection));
            }
        },

        selectTab:function(options){
            var configurationControls = TfgApp.model.get('controlsCollection').filter(function(item){
                return item.get('controlType') != 'light-control';
            });

            switch (options.tab) {
                case "go:system":
                    TfgApp.trigger("change:content", {
                        view:new TfgAppSystemView()
                    });
                    break;
                case "go:configuration":
                    TfgApp.trigger("change:content", {
                        view:new TfgAppConfigurationView({
                            configurationControlsCollection: configurationControls,
                            urlServer:TfgApp.model.get("serverUrl")
                        })
                    });
                    break;
            }
        },

        updateUrlServer:function(data){
            TfgApp.model.set("serverUrl", data.urlServer);
        },

        refresh:function(data){
            this.initializeData(data);
            TfgApp.trigger("change:content", {
                view:new TfgAppSystemView()
            });
        }
    });

    return Controller;
});
