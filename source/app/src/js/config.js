require.config({
    paths: {
        backbone: "../../bower_components/backbone/backbone",
        "backbone.babysitter": "../../bower_components/backbone.babysitter/lib/backbone.babysitter",
        "backbone.wreqr": "../../bower_components/backbone.wreqr/lib/backbone.wreqr",
        "backbone.marionette": "../../bower_components/backbone.marionette/lib/core/backbone.marionette",
        underscore: "../../bower_components/underscore/underscore",
        jquery: "../../bower_components/jquery/jquery",
        handlebars: "../../bower_components/handlebars/handlebars",
        "handlebars.runtime": "../../bower_components/handlebars/handlebars.runtime"
    },
    shim: {
        handlebars: {
            exports: "Handlebars"
        }
    }
});

require(["init"], function() {
    "use strict";
});