define([
    "app",
    "backbone.marionette",
    "underscore"
], function(TfgApp,
        Marionette,
        _){
    "use strict";

    var Model = Backbone.Model.extend({
        defaults: {
            status: ""
        }

    });

    return Model;
});
