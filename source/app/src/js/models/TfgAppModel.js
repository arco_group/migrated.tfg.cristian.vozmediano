define([
    "app",
    "backbone.marionette",
    "underscore"
], function(TfgApp,
        Marionette,
        _){
    "use strict";

    var Model = Backbone.Model.extend({
        defaults: {
            nameApp: "com.tfg.app",
            language: "es",
            serverUrl:"http://192.168.1.37:3000"
        }

    });

    return Model;
});
