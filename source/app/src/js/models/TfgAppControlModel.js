define([
    "app",
    "backbone.marionette",
    "underscore"
], function(TfgApp,
        Marionette,
        _){
    "use strict";

    var Model = Backbone.Model.extend({
        defaults: {
            controlType:"",
            label: "",
            place:"",
            status:""
        }

    });

    return Model;
});
