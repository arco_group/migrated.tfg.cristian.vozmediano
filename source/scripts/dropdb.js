var mongodb = require('mongodb'),
	config = require('../config/broker.config.json'),
	events;

    mongodb.MongoClient.connect(config.eventRegistry.iaRegistryDbUrl)
    .then(function(db) {
        db.collection('eventsCollection').deleteMany( {}, function(err, results) {
         console.log("Event collection empty");
		 process.exit(0);
      });
    });
