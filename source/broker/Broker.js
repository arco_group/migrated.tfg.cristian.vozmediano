/**
 * Requires
 */
var mosca = require('mosca'),
	mongodb = require('mongodb'),
	moment = require('moment'),
	_ = require('underscore'),
	config = require('../config/broker.config.json'),
	cvgMqttProtocol = require('../libs/cvgMqttProtocol'),
	devicesData = require('../libs/devices'),
	topicsDirectory = [],
	devices = [],
	broker,
	settings,
	events;

/*
 * Initialization
 */
settings = {
    port: config.port,
    backend: config.backend
};


/*
 * Interface for http server
 */
module.exports =  {
	run:function(){
		broker = new mosca.Server(settings);

		broker.on('clientConnected', function (client) {
		    console.log('Client connected', client.id);
		});

		broker.on('published', function (packet, client) {
			if(this.operative && client){
				console.log("Sistema operative -> ", this.operative);
				console.log('Published',packet.topic + '  /  ' + packet.payload.toString());
				var message = packet.payload.toString();
		        events.insert({
		        	time: moment().format("DD/MM/YYYY HH:mm:ss"),
		        	topic:packet.topic,
		        	message
		        });

				cvgMqttProtocol.publish(this, packet, client, topicsDirectory);
				_saveDevicesState(packet.topic, packet.payload.toString());
			}
		});

		broker.on('ready', function(){
			this.operative = true;

			mongodb.MongoClient.connect(config.eventRegistry.iaRegistryDbUrl)
		    .then(function(db) {
		    	return db.createCollection('eventsCollection');
		    }).then(function(_events){
				events = _events;
			})
		});

		broker.on('subscribed', function(topic) {
		    if (topicsDirectory.indexOf(topic) === -1) topicsDirectory.push(topic);
		});

		_initDevices();

		return broker;
	},

	system:function(options){
		var message;

		if(options && options.action){
			switch(options.action){
				case "on":
					broker.operative = true;
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'ON');
					console.log(message);
					broker.publish(message);
					console.log("System Operative");
					break;
				case "off":
					broker.operative = false;
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'OFF');
					broker.publish(message);
					console.log("System No Operative");
					break;
			}
		}else{
			return {
				status:broker.operative ? "on" : "off",
				devices: devices
			}
		}
	},

	lightControl:function(options){
		if(options.action){
			switch(options.action){
				case "off":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/light/house1/' + options.place, 'LOW');
					broker.publish(message);
					console.log("Light Control actuated from mobile app LOW");
					break;
				case "on":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/light/house1/' + options.place, 'HIGH');
					broker.publish(message);
					console.log("Encender luces -> ", 'cvgmqttprotocol/light/house1/' + options.place);
					console.log("Light Control actuated from mobile app HIHG");
					break;
			}
		}else{
			return {
				status:"on"
			};
		}
	},

	sensorlightControl:function(options){
		if(options.action){
			switch(options.place){
				case "downfloor":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'SENSORLIGHT_DOWNFLOOR');
					broker.publish(message);
					_updateState("sensorlight-control", options.place, options.action);
					console.log("Sensor Light Activate DOWNFLOOR");
					break;
				case "upfloor":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'SENSORLIGHT_UPFLOOR');
					broker.publish(message);
					_updateState("sensorlight-control", options.place, options.action);
					console.log("Sensor Light Activate DOWNFLOOR");
					break;
				case "all":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'SENSORLIGHT_ALL');
					broker.publish(message);
					_updateState("sensorlight-control", options.place, options.action);
					console.log("Sensor Light Activate DOWNFLOOR");
					break;
				case "none":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'SENSORLIGHT_OFF');
					broker.publish(message);
					_updateState("sensorlight-control", options.place, options.action);
					console.log("Sensor Light Deactivate");
					break;
			}
		}else{
			return {
				status:"on"
			};
		}
	},

	presenceControl:function(options){
		if(options.action){
			switch(options.action){
				case "off":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'SENSORPRESENCE_' + options.place.toUpperCase() + '_OFF');
					broker.publish(message);
					_updateState("presence-control", options.place, "off");
					console.log("Presence Control actuated from mobile app LOW");
					break;
				case "on":
					message = cvgMqttProtocol.createMessage('cvgmqttprotocol/system', 'SENSORPRESENCE_' + options.place.toUpperCase() + '_ON');
					broker.publish(message);
					_updateState("presence-control", options.place, "on");
					console.log("Presence Control actuated from mobile app HIHG");
					break;
			}
		}else{
			return {
				status:"on"
			};
		}
	}
}

function _saveDevicesState(topic, message){
	switch(topic){
		case "cvgmqttprotocol/system":
			if(message === "LIGHTCHANGE_DOWNFLOOR/HALL_ON"){
				_updateState("light-control", "downfloor/hall", "on");
			}else if(message === "LIGHTCHANGE_DOWNFLOOR/HALL_OFF"){
				_updateState("light-control", "downfloor/hall", "off");
			}else if(message === "LIGHTCHANGE_DOWNFLOOR/LIVINGROOM_ON"){
				_updateState("light-control", "downfloor/livingroom", "on");
			}else if(message === "LIGHTCHANGE_DOWNFLOOR/LIVINGROOM_OFF"){
				_updateState("light-control", "downfloor/livingroom", "off");
			}else if(message === "LIGHTCHANGE_DOWNFLOOR/KITCHEN_ON"){
				_updateState("light-control", "downfloor/kitchen", "on");
			}else if(message === "LIGHTCHANGE_DOWNFLOOR/KITCHEN_OFF"){
				_updateState("light-control", "downfloor/kitchen", "off");
			}else if(message === "LIGHTCHANGE_UPFLOOR/BATHROOM_ON"){
				_updateState("light-control", "upfloor/bathroom", "on");
			}else if(message === "LIGHTCHANGE_UPFLOOR/BATHROOM_OFF"){
				_updateState("light-control", "upfloor/bathroom", "off");
			}else if(message === "LIGHTCHANGE_UPFLOOR/BEDROOM_ON"){
				_updateState("light-control", "upfloor/bedroom", "on");
			}else if(message === "LIGHTCHANGE_UPFLOOR/BEDROOM_OFF"){
				_updateState("light-control", "upfloor/bedroom", "off");
			}else if(message === "LIGHTCHANGE_UPFLOOR/STUDY_ON"){
				_updateState("light-control", "upfloor/study", "on");
			}else if(message === "LIGHTCHANGE_UPFLOOR/STUDY_OFF"){
				_updateState("light-control", "upfloor/study", "off");
			}
			break;
	}

};

function _updateState(deviceType, place, state){
	var device;

	if(deviceType === "sensorlight-control"){
		device = _.find(devices, {controlType:"sensorlight-control"});
		if(device){
			device.place = place;
			if(place === "none"){
				device.status = "off";
			}else{
				device.status = "on";
			}
		}
	}else{
		device = _.find(devices, {controlType: deviceType, place: place});
		if(device) device.status = state;
	}



};

function _initDevices(){
	devices = devicesData.collection;
};
