module.exports = {
    "observations": {
        "HALL":0,
        "BEDROOM" : 1,
        "LIVING_ROOM": 2,
        "KITCHEN": 3,
        "BATHROOM": 4,
        "STUDY": 5,
        "LAUNDRY_ROOM": 6
    },

    "states": {
        "ENTER_HOME":0,
        "LEAVING_HOME":1,
        "READING": 2,
        "WATCH_TV": 3,
        "COOK": 4,
        "GET_SHOWER": 5,
        "BRUSH_TEETH": 6,
        "EATING":7,
        "SLEEPING": 8,
        "LAUNDRY": 9,
        "IRON": 10
    },

    //["ENTER_HOME", "READING", "SLEEPING", "WATCH_TV", "BRUSH_TEETH", "LAUNDRY", "IRON", "GET_SHOWER", "COOK", "EATING", "LEAVING_HOME"]
    "statesStartProbability": [0.2, 0.05, 0.1, 0.05, 0.1, 0.05, 0.1, 0.05, 0.2, 0.05, 0.05],

    "transitionProbabilityMatrix":[
        [0.0, 0.1, 0.0, 0.1, 0.2, 0.0, 0.0, 0.4, 0.2, 0.0, 0.0],    // ENTER_HOME
        [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],    // LEAVING_HOME
        [0.0, 0.0, 0.0, 0.1, 0.0, 0.2, 0.0, 0.0, 0.7, 0.0, 0.0],    // READING
        [0.0, 0.0, 0.0, 0.0, 0.1, 0.2, 0.0, 0.1, 0.6, 0.0, 0.0],    // WATCH_TV
        [0.0, 0.0, 0.1, 0.0, 0.0, 0.2, 0.0, 0.6, 0.0, 0.0, 0.1],    // COOK
        [0.0, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.5, 0.1, 0.0],    // GET_SHOWER
        [0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 0.8, 0.0, 0.0],    // BRUSH_TEETH
        [0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.7, 0.0, 0.0, 0.1, 0.0],    // EATING
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8, 0.0, 0.2, 0.0],    // SLEEPING
        [0.0, 0.0, 0.2, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6],    // LAUNDRY
        [0.0, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4, 0.2, 0.0, 0.0]     // IRON
    ],

    "emmisionProbabilityMatrix":[
        [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],    // ENTER_HOME : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],    // LEAVING_HOME : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.5, 0.2, 0.0, 0.0, 0.3, 0.0],    // READING : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.5, 0.3, 0.2, 0.0, 0.0, 0.0],    // WATCH_TV : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0],    // COOK : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],    // GET_SHOWER : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],    // BRUSH_TEETH : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.0, 0.7, 0.3, 0.0, 0.0, 0.0],    // EATING : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.8, 0.2, 0.0, 0.0, 0.0, 0.0],    // SLEEPING : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],    // LAUNDRY : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"],
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]     // IRON : ["HALL","BEDROOM", "LIVING_ROOM", "KITCHEN", "BATHROOM", "STUDY", "LAUNDRY_ROOM"]
    ]
}
