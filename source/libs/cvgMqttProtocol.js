/*
 * cvgMqttProtocol
 * author: Cristian Vozmediano Garcia
 * Library for own mqtt protocol on mqtt third-party library
 */

var _ = require ('underscore');

var DEVICES = ["SYSTEM", "LIGHT", "SENSORPRESENCE", "SWITCH", "BUZZER",
	"TEMPERATURE", "SENSORLIGHT", "RELAY", "TRACKING", "LASEREMIT",
	"PHOTORESISTOR", "HUMIDITY", "HALLMAGNETIC", "IREMISSION", "IRRECEIVER",
	"TOUCH", "JOYSTICK"];
	PROTOCOL_NAME = 'cvgmqttprotocol',
	TOPIC_REGEXP_STRUCTURE = /cvgmqttprotocol\/[a-zA-Z0-9]+\/*[a-zA-Z0-9]*/;

module.exports =  {
	/*
	 * Function for create a valid packet if data is valid. By default the
	 * qos (quality of service) is 0(it is the minimum level but guarantees a
	 * better delivery effort)and retains false (because we only want
	 * to receive the events from which we connect)
	 */
	createMessage:function(topic, message){
		if(_checkIfValidMessage(topic, message)){
			return {
				topic: topic,
			    payload: message,
			    qos: 0,
			    retain: false
			};
		}else{
			return null;
		}
	},

	/*
	 * Broker mosca library not working fine with wildcards. This function
	 * fixed this problem and control the packet with cvgMqttProtocol valid
	 * topic.
	 */
    publish: function(brokerContext, packet, client, topicsDirectory){
        if(TOPIC_REGEXP_STRUCTURE.test(packet.topic)){
            if (/\+|#/.test(packet.topic)) {
                var regex = new RegExp(packet.topic.replace('+', '[^/]+').replace('#', '.+'));
                for(var i = 0; i < topicsDirectory.length; i++) {
                    if (_checkRegExp(regex, topicsDirectory[i])) {
                        packet.topic = topicsDirectory[i];
						brokerContext.publish(packet);
                    }
                }

            }
        }
    }
}

/*
* Utils functions
*/

function _checkRegExp(regex, str){
	var regexResult = regex.exec(str);

	if(regexResult){
		return regex.exec(str)[0] === str;
	}else{
		return false;
	}
};

function _checkIfValidMessage(topic, message){
	var device, topicIsValid, messageIsValid;
	console.log("Topic ->",topic);
	console.log("Message ->",message);
	if(topic.indexOf("system") !== -1){
		device = "SYSTEM";
	}else{
		device = topic.substring(16,topic.indexOf('/',16)).toUpperCase();
	}
	console.log("Device ->", device)
	topicIsValid = _.contains(DEVICES, device);
	messageIsValid = typeof message === "string";

	return topicIsValid && messageIsValid;
}
