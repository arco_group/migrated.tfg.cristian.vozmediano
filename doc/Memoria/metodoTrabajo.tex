\chapter{Método y Fases de Trabajo}
\label{chap:fases}

\drop{E}{n} este capítulo describiremos el método de trabajo utilizado para la realización de este proyecto. En particular para este desarrollo se ha utilizado una de las llamadas metodologías ágiles~\cite{MetodologiasAgiles}.

Las metodologías ágiles son una serie de técnicas para la gestión de proyectos que surgieron como contraposición a las metodologías de gestión clásicas. Con estas metodologías se minimiza el impacto de las tareas que no son imprescindibles para llegar a la consecución del proyecto, aumentando la eficiencia de las personas implicadas y reduciendo el coste.

Las metodologías clásicas son menos efectivas en proyectos donde los requisitos y el entorno es susceptible a cambios. Con las metodologías ágiles, ya se contempla este tipo de cambios, ofreciendo una mejor respuesta que las metodologías clásicas. Nos podemos hacer la pregunta de que si las metodologías ágiles son mejores que las clásicas o viceversa y la respuesta es que no, como en otros aspectos de la vida, depende del tipo de proyecto en el que se vayan a aplicar.

Para considerar que una metodología es ágil deben cumplir \textit{«El Manifiesto Ágil»}~\cite{ManifiestoAgil}:

\begin{itemize}
  \item Los individuos y su interacción por encima de los procesos y las herramientas.
  \item El software que funciona mejor que la documentación exhaustiva.
  \item La colaboración con el cliente por encima de la negociación contractual.
  \item La respuesta al cambio por encima del seguimiento de un plan.
\end{itemize}


Algunas de las metodologías ágiles más utilizadas son las siguientes:

\begin{itemize}
\item SCRUM~\cite{SCRUM}. Entorno de trabajo que proporciona herramientas y roles para, de manera iterativa, llegar a obtener resultados de un proyecto.
\item XP~\cite{XP}. Metodología centrada en potenciar las relaciones interpersonales para llegar al éxito en desarrollo de software, promoviendo el trabajo en equipo.
\item KANBAN~\cite{KANBAN}. Basada en una idea muy simple, que el trabajo en curso debe acotarse y dividirse de manera que hasta que un bloque de trabajo no está terminado no se continúa con el siguiente.
\end{itemize}

Para el desarrollo de este proyecto, por la naturaleza del mismo, el número de personas que formarían el equipo de desarrollo y el número de desarrolladores, se ha optado por elegir Scrum.

\section{SCRUM}

Esta metodología sigue la estrategia de desarrollo iterativo e incremental. Se van definiendo bloques que se llevan a cabo en cada iteración o \textit{«Sprint»}, generando una versión del producto que aporta valor al cliente. Se muestra este producto al cliente al final de cada sprint para que se pueda refinar y tomar decisiones sobre posibles modificaciones para futuras iteraciones.

Las características más representativas son:

\begin{itemize}
  \item Gestión de las expectativas de los clientes.
  \item Resultados tempranos.
  \item Flexibilidad y adaptación.
  \item Retorno de la inversión (valor).
  \item Mitigación de riesgos.
  \item Productividad y calidad.
  \item Motivación del equipo de desarrollo.
\end{itemize}

Existen diferentes maneras de implementar los sistemas para gestionar el proceso Scrum, como tablones con \textit{«post-it»}, pizarras o soluciones software como \textit{«Trello»}.

\subsection{Roles}
En esta metodología se definen una serie de roles que deben formalizarse y cada cual tiene su cometido en el proceso. Estos roles son:

\begin{itemize}
  \item \textit{Product Owner} o dueño del producto, la persona autorizada para definir el conocimiento funcional del proyecto. Representa al cliente, usuarios del software y en definitiva a todas las partes implicadas en el producto. Entre sus funciones están las de canalizar la lógica de negocio y transmitirlas al equipo en objetos de valor para el producto, revisar el producto para ir mejorando las funcionalidades y aportar mayor valor al negocio. En este proyecto serían los directores \textit{María José Santofimia Romero} y \textit{Félix Jesús Villanueva Molina}.
  \item \textit{Scrum Master}, lejos de ser el líder del equipo, es el facilitador que protege al equipo y vela por que se apliquen los principios ágiles del Scrum. Debe incentivar y motivar al equipo, resolver conflictos que bloqueen el progreso del proyecto, ayudar al equipo y hacer prevalecer la separación entre el equipo y el resto de participantes del proyecto. En este proyecto serían los directores \textit{María José Santofimia Romero} y \textit{Félix Jesús Villanueva Molina}.
  \item \textit{Scrum Team}, es el equipo de desarrolladores propiamente dicho. Son personas técnicas cualificadas que llevan a cabo el desarrollo de software del producto. En este proyecto sería el autor del proyecto \textit{Cristian Vozmediano García}
\end{itemize}

También existe otro tipo de roles auxiliares que se denominan \textit{«Stakeholders»} que suelen ser clientes, proveedores y demás personas que hacen posible el proyecto y para los que este proyecto les repercutirá con algún beneficio, el cuál justifica su desarrollo.

\subsection{Fases}

Esta metodología se desarrolla en tres fases (ver figura~\ref{fig:fasesScrum}) que describimos a continuación:

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.4\textwidth]{fases-scrum.png}
\caption{Fases de la metodología Scrum}
\label{fig:fasesScrum}
\end{center}
\end{figure}


\begin{itemize}
  \item \textit{Pre-Game}. En esta fase se desarrolla la planificación, donde se presenta al equipo el \textit{«Product Backlog»}, lista de requisitos y funcionalidades que debe desarrollarse para el proyecto por orden de prioridad. Después, se lleva a cabo el plan para implementar los requisitos teniendo en cuenta las posibles dependencias entre ellas y la definición de la arquitectura del sistema a alto nivel.
  \item \textit{Game}. En esta fase se llevan a cabo los \textit{«Sprints»}(ver figura~\ref{fig:cicloScrum}) de desarrollo. La duración de cada sprint es determinada por el equipo y suelen variar entre dos y cuatro semanas normalmente. En cada sprint o iteración, se lleva a cabo el desarrollo de nuevas funcionalidades para tener una versión que aporte valor al cliente. Dentro de los sprints se hace una realimentación debido a ceremonias como las \textit{«Dailys»}\footnote{Reuniones diarias normalmente a primera hora para analizar el progreso del día anterior}.
  \item \textit{Post-Game}. Una vez se termina el desarrollo del sprint, se presenta el producto con su documentación y pruebas al cliente y se analizan las funcionalidades desarrolladas. Esto se hace en el \textit{«Sprint Review»}, donde el cliente también puede pedir que se realicen cambios o modificaciones, que se planificarán para futuros sprints.
\end{itemize}


\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.8\textwidth]{ciclo-sprint.jpg}
\caption{Fases de un ciclo de sprint\cite{CicloSprint}}
\label{fig:cicloScrum}
\end{center}
\end{figure}

\subsection{Metodología Scrum aplicada a este proyecto}

A continuación describiremos las fases de scrum aplicadas a la realización de este proyecto.

En primer lugar, en la fase \textit{pre-game} se acordó con el Product Owner cuáles eran las necesidades y las características que debía tener el producto a desarrollar y el objetivo del mismo. Se debía construir una plataforma para la monitorización y control de sistemas inteligentes, minimizando los costes, siendo escalable para poder añadir más controles de manera sencilla. Además debía servir para ayudar en el día a día de personas ancianas gracias precisamente a la monitorización y el control de dichos sistemas inteligentes, como puede ser un entorno domótico. Por lo tanto se definió el Backlog de la siguiente manera:

\begin{itemize}
  \item Análisis y estudio del estado del arte.
  \item Análisis de las tecnologías de desarrollo para el desarrollo del sistema.
  \item Estudio del protocolo de comunicación para los sistemas.
  \item Desarrollo servidor de protocolo de comunicación.
  \item Montaje circuitos de los sistemas sensores y actuadores.
  \item Desarrollo servidor web para la conexión del sistema con el exterior.
  \item Desarrollo aplicación móvil para los usuarios.
  \item Implementación módulo de inteligencia.
  \item Montaje de servidores y tareas de construcción, despliegue y mantenimiento del sistema.
\end{itemize}

Después en la fase de \textit{game} se llevaron a cabo los sprints para el desarrollo del sistema, desarrollando cada parte en orden de prioridad del Backlog.


  \subsubsection{Sprint 1: Análisis y estudio del estado del arte}
    \begin{itemize}
      \item \textit{Planificación:} Analizar y realizar un estado del arte para decidir como será el sistema.
      \item \textit{Análisis de requisitos:} Debe ser un sistema que cumpla el objetivo del proyecto teniendo en cuenta que servirá de prototipo para sistemas futuros.
      \item \textit{Diseño:} Se estudian los proyectos y trabajos existentes, mirando que cosas se pueden mejorar y aportar con este sistema.
      \item \textit{Implementación:} Se realiza \textit{«Brainstorming»} una vez analizado el estado del arte y se definen las partes de las que estará formado el sistema (arquitectura).
      \item \textit{Pruebas:} Se comprueba que la arquitectura del sistema abarca y aporta la funcionalidad requerida.
    \end{itemize}

  \subsubsection{Sprint 2: Análisis de las tecnologías de desarrollo para el desarrollo del sistema}
    \begin{itemize}
      \item \textit{Planificación:} Se analizan las tecnologías de desarrollo del estado del arte y otras que pueden encajar para el desarrollo del sistema.
      \item \textit{Análisis de requisitos:} Las tecnologías deben ser conocidas por el desarrollador para que el desarrollo sea más rápido y eficiente y que permitan la implementación del sistema teniendo en cuenta el ámbito de aplicación de cada una de ellas y del sistema que se va a desarrollar.
      \item \textit{Diseño:} Se muestra algunas tablas comparativas.
      \item \textit{Implementación:} Se realiza una comparativa de las tecnologías.
      \item \textit{Pruebas:} Se comprueba que las tecnologías escogidas son las idóneas para llevar a cabo la implementación del sistema.
    \end{itemize}


  \subsubsection{Sprint 3: Estudio del protocolo de comunicación para los sistemas}
    \begin{itemize}
      \item \textit{Planificación:} Se deben estudiar los protocolos de comunicación que existen y que son susceptibles a ser elegidos.
      \item \textit{Análisis de requisitos:} El protocolo debe ser estándar y que no sea compleja su implementación.
      \item \textit{Diseño:} Se crean diagramas de los protocolos estudiados para entender su funcionamiento.
      \item \textit{Implementación:} Se diseña el flujo de comunicación que tendrá el protocolo.
      \item \textit{Pruebas:} Se verifica que el protocolo elegido puede implementar todo el flujo de comunicación del sistema.
    \end{itemize}


  \subsubsection{Sprint 4: Desarrollo servidor de protocolo de comunicación}
    \begin{itemize}
      \item \textit{Planificación:} Se tiene que implementar el servidor de comunicaciones.
      \item \textit{Análisis de requisitos:} El servidor de comunicaciones, debe ser escalable, de manera que sea relativamente sencillo suscribir más controles.
      \item \textit{Diseño:} Se realizan diagramas, para el mejor entendimiento del flujo de eventos que se darán en la comunicación de los clientes con el servidor.
      \item \textit{Implementación:} Utilizando una librería externa que implementa \acs{MQTT}, implementamos el protocolo domótico sobre \acs{MQTT} (que es utilizado para distribuir la información).
      \item \textit{Pruebas:} Se verifica que el protocolo funciona y que la comunicación entre los elementos del sistema es correcta.
    \end{itemize}


  \subsubsection{Sprint 5: Montaje circuitos de los sistemas sensores y actuadores}
    \begin{itemize}
      \item \textit{Planificación}: Se debe montar el circuito utilizando dos placas Arduino e implementar los programas de control de cada placa. Para ello se utiliza lenguaje \acs{JS} y una librería de terceros para poder ejecutar código de este lenguaje en las placas. A parte del programa de control, también implementan clientes \acs{MQTT} con otra librería de terceros.
      \item \textit{Análisis de requisitos:} El circuito será una representación básica de una casa con dos puntos de luz, un sensor de presencia y otro de luminosidad y dos interruptores.
      \item \textit{Diseño:} Se crea un diagrama con el esquema de montaje del circuito y diagrama de secuencia del funcionamiento del mismo.
      \item \textit{Implementación:} Se monta el circuito y se desarrollan los programas de control.
      \item \textit{Pruebas:} Se verifica que el circuito funciona correctamente.
    \end{itemize}

  \subsubsection{Sprint 6: Desarrollo servidor web para la conexión del sistema con el exterior}
    \begin{itemize}
      \item \textit{Planificación:} Consta de dos tareas principalmente, implementar el \acs{API} \acs{REST} y asociar cada una de las llamadas con el servidor de comunicaciones.
      \item \textit{Análisis de requisitos:} El servidor debe exponer un \acs{API}, para recibir y mandar datos con las aplicaciones clientes y servir de punto de entrada/salida del servidor de comunicaciones.
      \item \textit{Diseño:} Se realiza esquema de funcionamiento.
      \item \textit{Implementación:} Se desarrolla el servidor con \acs{JS} y \textit{«Nodejs»}.
      \item \textit{Pruebas:} Se verifica mediante llamadas a la \acs{API} que el servidor funciona correctamente.
    \end{itemize}


  \subsubsection{Sprint 7: Desarrollo aplicación móvil para los usuarios}
    \begin{itemize}
      \item \textit{Planificación:} Se plantea las diferentes partes que hay que desarrollar para la aplicación móvil y se dividen las tareas en consecuencia. La aplicación consta de tres vistas principales por lo que las tareas de desarrollo se dividió en cada una de las vistas y además otra para la implementación de las llamadas al \acs{API}.
      \item \textit{Análisis de requisitos:} La aplicación debe mostrar una vista con la información del sistema global y otra con cada uno de los sistemas de control conectados, además de poder controlarlos desde la misma vista. Y la tercera vista debe ser para la simulación del funcionamiento del sistema.
      \item \textit{Diseño:} Se crea un esquema de como debe ser la aplicación a nivel de interfaz de usuario.
      \item \textit{Implementación:} Se desarrolla una aplicación web con \acs{HTML}, \acs{JS} y \acs{CSS}. Después se monta sobre un contenedor mediante librería de terceros \textit{«Apache Cordova»} para su instalación y ejecución en los dispositivos móviles Android.
      \item \textit{Pruebas:} Se verifica que la aplicación se puede ejecutar en Android y que el funcionamiento y la información de las vistas es el correcto.
    \end{itemize}

  \subsubsection{Sprint 8: Implementación módulo de inteligencia}
    \begin{itemize}
      \item \textit{Planificación:} Implementación de un módulo de inteligencia con algoritmos de inteligencia artificial para el análisis y explotación de datos y predicción de acciones.
      \item \textit{Análisis de requisitos:} El módulo debe recoger los datos de la base de datos y explotarlos haciendo los procesados necesarios además realizar la predicción de posibles acciones futuras a realizar por el usuario.
      \item \textit{Diseño:} Se realiza diagrama del funcionamiento del módulo para su mejor entendimiento.
      \item \textit{Implementación:} Se implementa el módulo mediante lenguaje \acs{JS}. Se implementa un módulo usando diferentes funciones para ir procesando los datos de manera que se pueda
      después ejecutar un algoritmo para la predicción de acciones futuras utilizando un \acs{HMM} a través de una librería llamada \textit{Node HMM}.
      \item \textit{Pruebas:} Se valida que el sistema recoge la información, la procesa y se obtienen predicciones en base a dicha información.
    \end{itemize}

  \subsubsection{Sprint 9: Montaje de servidores y tareas de construcción, despliegue del sistema y mejoras}
    \begin{itemize}
      \item \textit{Planificación:} Para que la construcción del sistema sea lo más adaptable posible y se pueda hacer de manera sencilla, se deben construir scripts de construcción y despliegue.
      \item \textit{Análisis de requisitos:} Los scripts deben poder dar la capacidad de construir el sistema con facilidad y dando opciones de configuración. Se tiene que poder ejecutar en el hardware de bajo coste propuesto. También se llevan a tareas de mejoras del sistema en general.
      \item \textit{Diseño:} Se realiza una tabla con las opciones que podrían tener los scripts.
      \item \textit{Implementación:} Los scripts se desarrollan para la ejecución con la herramienta \acs{NPM}\footnote{https://www.npmjs.com/}.
      \item \textit{Pruebas:} Se verifica que las tareas de construcción y despliegue funcionan correctamente y el sistema se crea acorde las opciones introducidas.
    \end{itemize}

Por último, la fase \textit{post-game}, en el \textit{«Sprint Review} se mostró al cliente lo realizado en cada iteración de la siguiente manera:

  \subsubsection{Sprint 1: Análisis y estudio del estado del arte}
    Este sprint se desarrolló en 2 semanas y posteriormente se mostró al cliente para revisar el estado del arte y poder decidir si continuar con la siguiente iteración.

  \subsubsection{Sprint 2: Análisis de las tecnologías de desarrollo para el desarrollo del sistema}
    Se llevó a cabo el análisis un 1 semana y se verificó la viabilidad del desarrollo del sistema con las tecnologías y herramientas estudiadas.

  \subsubsection{Sprint 3: Estudio del protocolo de comunicación para los sistemas}
    El análisis de los protocolos de comunicación se llevó a cabo en 2 semanas. Hubo algún contratiempo por lo que duró algo más de lo esperado. Al final se optó por el protocolo \acs{MQTT}.

  \subsubsection{Sprint 4: Desarrollo servidor de protocolo de comunicación}
    Este sprint se desarrolló en 2 semanas y se hicieron pruebas mediante un programa de prueba. Se mostró el funcionamiento del protocolo y se aceptó.

  \subsubsection{Sprint 5: Montaje circuitos de los sistemas sensores y actuadores}
    El montaje del circuito, duró 1 semana, aunque hubo que modificarlo en sprints posteriores. Se verificó la primera aproximación que se tenía aunque después se mejorara.

  \subsubsection{Sprint 6: Desarrollo servidor web para la conexión del sistema con el exterior}
    Se realizó el servidor web en 1 semana y se mostró probándolo con la herramienta \textit{POSTMAN}\footnote{https://www.getpostman.com/} que nos permite lanzar llamadas a un \acs{API}.

  \subsubsection{Sprint 7: Desarrollo aplicación móvil para los usuarios}.
    La aplicación móvil se tardó 3 semanas, pues hubo algún cambio necesario a mitad de sprint, debido al cambio en el circuito. Se probó contra el sistema para mostrarlo al cliente.

  \subsubsection{Sprint 8: Implementación módulo de inteligencia}
   Este sprint se llevo a cabo en 1 semana. Se analizó la necesidad de incorporar este módulo para añadirle robustez y un valor añadido al sistema.

  \subsubsection{Sprint 9: Montaje de servidores y tareas de construcción, despliegue del sistema y mejoras}
  Se llevó a cabo en 3 semanas, pues coincidiendo con el final del \acs{TFG} hubo que mejorar algunas partes del sistema y añadir alguna funcionalidad para dotar al sistema de una mejor aplicación a un entorno real.


  \section{Tecnologías de desarrollo}

  En las tecnologías de desarrollo existen infinidad de paradigmas, lenguajes de programación y herramientas para desarrollar. Estas tecnologías se tienen que centrar en el tipo de sistema que se va a desarrollar. La plataforma que se va a crear tiene partes distintas pero que de alguna manera se relacionan y se complementan entre ellas, por lo que se utilizarán diferentes tecnologías de desarrollo de distinta índole.

  En el apartado hardware, el arduino, como se ha comentado tiene un \acs{IDE} de desarrollo con un lenguaje de programación basado en Wiring\footnote{Entorno de programación de código abierto para entradas/salidas.}. Nosotros optaremos por lenguaje \acs{JS}, ya que como se comentará en las herramientas software utilizadas, han surgido algunas bibliotecas en este lenguaje para ejecutar programas escritos en él, dentro de la placa arduino.

  Para el desarrollo del servidor web y control domótico, también se puede utilizar \acs{JS}. Javascript ha evolucionado mucho recientemente y ha dejado de ser un lenguaje de programación para enriquecer las páginas web. Ahora se ha convertido en un lenguaje utilizado en un amplio stack tecnológico que va desde el \textit{backend} hasta el \textit{frontend}, pasando por la programación para la utilización de bases de datos y, como se ha mencionado anteriormente, para programación de hardware.

  Por último para la aplicación móvil, se estudió desarrollarla en lenguaje Java\footnote{https://www.java.com/es/} para el sistema operativo Android, pero se desestimó por el tiempo de aprendizaje que llevaría y además nos limitaría a un sólo sistema operativo. De esta manera, surgió la idea de hacer una aplicación móvil híbrida, que no deja de ser una aplicación web típica pero alojada en un contenedor nativo para distintos sistemas operativos. Este tipo de aplicaciones está bastante en auge ya que se pueden crear aplicaciones multiplataforma basada en \textit{HTML5} de una manera rápida y sencilla.



\section{Herramientas}

Daremos a conocer las herramientas técnicas, tanto software como hardware que se han utilizado para el desarrollo e implementación de este \acs{TFG}.

\subsection{Medios hardware}

Se ha utilizado hardware para el desarrollo del sistema y para crear el sistema en sí (circuitos).

\begin{itemize}
    \item Computador de sobremesa para el desarrollo del \acs{TFG} con Procesador Intel\textregistered Core\texttrademark i7 CPU 870 2.93GHz 8GB Memoria RAM DDR3 y 1TB HDD. Desarrollo del sistema y documentación.
    \item MacBook Pro con Procesador Intel\textregistered Core\texttrademark i5 2.5GHz 8GB Memoria RAM DDR3 y 128GB SSD. Desarrollo del sistema.
    \item Móvil Android. Dispositivo móvil para la realización de pruebas del sistema implementado. Móvil LG G4, Android 6.0.1.
      \item Raspberry PI Model B. Procesador ARM 1176JZF-S, Memoria RAM 512MB. Homebase del sistema.
      \item Dos Arduino Modelo UNO. Implementación de circuitos sensores y actuadores.
\end{itemize}

\subsection{Medios software}

Se ha utilizado software tanto para el desarrollo del \acs{TFG}, como para para la implementación del sistema (frameworks y bibliotecas).

\begin{itemize}
  \item Sistemas Operativos
    \begin{itemize}
      \item Android\footnote{http://www.android.com/}. Sistema operativo móvil sobre el que funcionan los dispositivos móviles o tablets donde se ejecutará el sistema implementado. Dichos dispositivos son muy accesibles hoy en día y tienen una capacidad de cómputo interesante. Son fáciles de transportar y alcanzables para cualquier tipo de usuario.
        \item Mac OS X El Capitán\footnote{http://www.apple.com/es/osx/}. Sistema operativo donde se realizará el desarrollo del sistema.
        \item Debian 8 Jessie\footnote{https://www.debian.org/index.es.html}. Sistema operativo para realizar desarrollo del sistema y la documentación.
        \item Raspbian Jessie Lite\footnote{https://www.raspbian.org/}. Sistema operativo para el servidor donde se correrá el homebase del sistema.
    \end{itemize}


  \item Lenguajes
    \begin{itemize}
      \item Javascript\footnote{http://www.w3schools.com/js/}. Lenguaje de programación para el desarrollo del sistema.
      \item \LaTeX{}\footnote{https://www.latex-project.org/}. Lenguaje para el desarrollo de la documentación.
    \end{itemize}


  \item Frameworks y Bibliotecas
    \begin{itemize}
      \item Nodejs\footnote{https://nodejs.org/en/}. Librería sobre lenguaje Javascript para ejecutar en el servidor.
      \item Jonnhy-Five\footnote{http://johnny-five.io/}. Librería para desarrollo con Arduino mediante lenguaje Javascript.
      \item Mosca\footnote{http://www.mosca.io/}. Broker MQTT.
      \item MQTT.js\footnote{https://www.npmjs.com/package/mqtt}. Cliente MQTT.
      \item Apache Cordova\footnote{https://cordova.apache.org/}. Framework para creación de aplicaciones móviles híbridas con tecnología web como HTML5.
      \item BackboneJS\footnote{http://backbonejs.org/}. Framework MVC para desarrollo de aplicación web con Javascript.
      \item MarionetteJS\footnote{https://angularjs.org/}. Framework para mejorar y simplificar el desarrollo de aplicaciones con BackboneJS, aportando sistema de vistas y solución arquitectónica de software.
      \item MongoDB\footnote{https://www.mongodb.com/es}. Base de datos no relacional, de tipo documental para el almacenamiento de la información que se genera en el entorno inteligente.
      \item Node HMM\footnote{https://www.npmjs.com/package/nodehmm}. Biblioteca con la implementación del Modelo Oculto de Markov.
    \end{itemize}

  \item Herramientas
    \begin{itemize}
      \item NPM\footnote{https://www.npmjs.com/}. Gestor de paquetes para la instalación de bibliotecas y dependencias para el desarrollo.
      \item Atom \footnote{https://atom.io/}. Editor de código para programar el sistema.
      \item Mercurial\footnote{https://www.mercurial-scm.org/}. Sistema de control de versiones para el seguimiento de la evolución del código fuente del sistema y de las versiones del mismo.
    \end{itemize}

\end{itemize}






% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
